# gek

## Random KDE Plasma color scheme generator

This bash script generates a random color scheme for KDE Plasma.
It can apply the color scheme using [plasma-theme-switcher](https://github.com/maldoinc/plasma-theme-switcher), but it can also export it as a .colors-file.

Note: These color schemes aren't meant to look good, I just made this for fun.
## Dependencies

Needed:
- bash (This script uses some bash-specific features, so other shells might not work)
- sed
- shuf
- awk

These packages should already be installed on most Linux distributions.

Recommended:
- [plasma-theme-switcher](https://github.com/maldoinc/plasma-theme-switcher) (For applying the color scheme and using the -b option)
- KDE Plasma (To actually use the color scheme)

## Installation

First clone this repo:

`git clone https://codeberg.org/Bastindo/gek`

Then use make to install gek:

`sudo make install`

## Usage

```
Usage: gek [-o FILE] [-a] [-b]
Generator for random KDE color schemes

Options:
-h, --help              Display this help page
-v, --version           Display the version of this program
-o, --output <FILE>     Save color scheme to .colors file
-a, --apply             Apply color scheme
-b                      Switch to a color-scheme-respecting Plasma style (Breeze)
```

### Examples:

Generate a color scheme and apply it: `gek -a`

Generate a color scheme and save it to file.colors: `gek -o file.colors`

Generate a color scheme and save it to file.colors while also applying it: `gek -o file.colors -a`


